FROM alpine:3.6

COPY java-validator.sh /

ENTRYPOINT ["/java-validator.sh"]

